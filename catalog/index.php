<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Каталог медицинских товаров и расходных материалов - цены, описание | GioMarket");
$APPLICATION->SetTitle("Каталог медицинских расходных материалов");

$current_link  = $APPLICATION->GetCurPage();
if($current_link == '/catalog/shpritsy/') {
    ?>
    <style>
        .bxr-ecommerce-v3-lite-color .bxr-element-container .bxr-element-name a {
            font-weight: normal;
        }
    </style>
    <?
}

$PAGE_ELEMENT_COUNT = 12;

if (isset($_GET['PAGE_ELEMENT_COUNT']))
	$_SESSION['PAGE_ELEMENT_COUNT'] = $_GET['PAGE_ELEMENT_COUNT'];

if (isset($_SESSION['PAGE_ELEMENT_COUNT']))
	$PAGE_ELEMENT_COUNT = $_SESSION['PAGE_ELEMENT_COUNT'];

if ($PAGE_ELEMENT_COUNT == 'all')
	$PAGE_ELEMENT_COUNT = 9999;


if (!isset($_GET['sort']))
{
	$_GET['sort'] = 'NAME';
	$_GET['order'] = 'ASC';
}

?><?$APPLICATION->IncludeComponent(
	"alexkova.market:catalog", 
	".default", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADDITIONAL_DETAIL_INFO" => "/include/detail_info.php",
		"ADDITIONAL_SKU_PIC_2_SLIDER" => "Y",
		"ADDITIONAL_TAB_NAME" => "",
		"ADDITIONAL_TAB_PATH" => "",
		"ADDITIONAL_TAB_SHOW" => "N",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_PICT_PROP" => "MORE_PHOTO",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"ALSO_BUY_ELEMENT_COUNT" => "4",
		"ALSO_BUY_MIN_BUYES" => "1",
		"ALSO_BUY_TITLE" => "С этим товаром покупают",
		"ANOUNCE_TRUNCATE_LEN" => "",
		"ARTICLE_POSITION" => "none",
		"BASKET_URL" => "/personal/basket/",
		"BESTSALLERS_CNT" => "3",
		"BESTSALLERS_SORT" => "200",
		"BESTSALLERS_TITLE" => "Популярные товары",
		"BESTSALLERS_WERE_SHOW" => "left",
		"BIG_DATA_CNT" => "3",
		"BIG_DATA_RCM_TYPE" => "similar_sell",
		"BIG_DATA_TITLE" => "Вам может также понадобиться",
		"BXR_GIFT_NOTICE_TEXT" => "",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "3600000",
		"CACHE_TYPE" => "A",
		"CATALOG_DEFAULT_SORT" => "",
		"CATALOG_DEFAULT_SORT_ORDER" => "desc",
		"CATALOG_VIEW_SHOW" => "N",
		"CHANGE_TITLE_SKU" => "N",
		"CHECK_DATES" => "N",
		"CHILD_MENU_TYPE" => "",
		"COMMON_ADD_TO_BASKET_ACTION" => "ADD",
		"COMMON_SHOW_CLOSE_POPUP" => "N",
		"COMPARE_ELEMENT_SORT_FIELD" => "shows",
		"COMPARE_ELEMENT_SORT_ORDER" => "asc",
		"COMPARE_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"COMPARE_OFFERS_FIELD_CODE" => array(
			0 => "NAME",
			1 => "",
		),
		"COMPARE_OFFERS_PROPERTY_CODE" => array(
			0 => "CML2_BAR_CODE",
			1 => "",
		),
		"COMPARE_POSITION" => "top left",
		"COMPARE_POSITION_FIXED" => "Y",
		"COMPARE_PROPERTY_CODE" => array(
			0 => "MATERIAL",
			1 => "MINIMUM_PRICE",
			2 => "MAXIMUM_PRICE",
			3 => "VIDEO_RESIZED",
			4 => "COLOR",
			5 => "MECH_KOL",
			6 => "SUVENIR_NAZNACH",
			7 => "GLASS_OS",
			8 => "VELO_NAGRUZKA",
			9 => "PAL_TYPE",
			10 => "VELO_TYPE",
			11 => "PLATFORM",
			12 => "COMPLECT",
			13 => "ARB_TIME",
			14 => "CAMERA_TYPE",
			15 => "GLASS_TYPE",
			16 => "DISPLAY_TYPE",
			17 => "ENERGY_TYPE",
			18 => "MATRIX_TYPE",
			19 => "CAMERA_POSITION",
			20 => "GLASS_CPU",
			21 => "DIAGONAL",
			22 => "ARB_CHANNEL",
			23 => "VOLUME",
			24 => "PX_SUMM",
			25 => "MATRIX",
			26 => "GLASS_ALL",
			27 => "OBJOM_VSTROENNOY_PAMYATI",
			28 => "SIZE_ACCUMUL",
			29 => "FORM_FACTOR",
			30 => "DYSPLAY_CAM",
			31 => "SUMM_SIM",
			32 => "CHASTOTA",
			33 => "PERSONAL_SIZE",
			34 => "FORMAT_SIM",
			35 => "IMPEDANS",
			36 => "",
		),
		"COMPARE_SCROLL_UP" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CONVERT_CURRENCY" => "Y",
		"CURRENCY_ID" => "RUB",
		"DEFAULT_CATALOG_VIEW" => "TITLE",
		"DELAY" => "Y",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "Y",
		"DETAIL_ADD_DETAIL_TO_SLIDER_SKU" => "N",
		"DETAIL_ADD_TO_BASKET_ACTION" => array(
		),
		"DETAIL_BACKGROUND_IMAGE" => "-",
		"DETAIL_BLOG_EMAIL_NOTIFY" => "N",
		"DETAIL_BLOG_URL" => "catalog_comments",
		"DETAIL_BLOG_USE" => "Y",
		"DETAIL_BRAND_PROP_CODE" => array(
			0 => "MANUFACTURER",
			1 => "",
		),
		"DETAIL_BRAND_USE" => "Y",
		"DETAIL_BROWSER_TITLE" => "-",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
		"DETAIL_DISPLAY_NAME" => "N",
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "H",
		"DETAIL_DISPLAY_SHOW_FILES" => "Y",
		"DETAIL_DISPLAY_SHOW_VIDEO" => "N",
		"DETAIL_FB_APP_ID" => "1419756338083528",
		"DETAIL_FB_USE" => "Y",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_OFFERS_FIELD_CODE" => array(
			0 => "ID",
			1 => "NAME",
			2 => "DETAIL_PICTURE",
			3 => "IBLOCK_ID",
			4 => "",
		),
		"DETAIL_OFFERS_PROPERTY_CODE" => array(
			0 => "PROIZVODITEL_1",
			1 => "CML2_ARTICLE",
			2 => "CML2_BASE_UNIT",
			3 => "MORE_PHOTO",
			4 => "MATERIAL",
			5 => "CML2_TRAITS",
			6 => "CML2_TAXES",
			7 => "FILES",
			8 => "CML2_ATTRIBUTES",
			9 => "CML2_BAR_CODE",
			10 => "OFFER_ID_HIDDEN",
			11 => "ID",
			12 => "SILENT",
			13 => "SIZE",
			14 => "COLOR",
			15 => "",
		),
		"DETAIL_PROPERTY_CODE" => array(
			0 => "PROIZVODITEL_1",
			1 => "CML2_ARTICLE",
			2 => "OTHER_ELEMENTS",
			3 => "MATERIAL",
			4 => "optom_vygodney",
			5 => "VOLUME",
			6 => "price_per_capita",
			7 => "country_of_manufacture",
			8 => "MATERIAL_2",
			9 => "VIDEO",
			10 => "OPT",
			11 => "MATERIAL_1",
			12 => "VIDEO_RESIZED",
			13 => "METAL_DETECTOR",
			14 => "INSULATION",
			15 => "COLOR",
			16 => "MECH_KOL",
			17 => "SUVENIR_NAZNACH",
			18 => "GLASS_OS",
			19 => "VELO_NAGRUZKA",
			20 => "PAL_TYPE",
			21 => "VELO_TYPE",
			22 => "PLATFORM",
			23 => "COMPLECT",
			24 => "ARB_TIME",
			25 => "CAMERA_TYPE",
			26 => "GLASS_TYPE",
			27 => "DISPLAY_TYPE",
			28 => "ENERGY_TYPE",
			29 => "MATRIX_TYPE",
			30 => "CAMERA_POSITION",
			31 => "GLASS_CPU",
			32 => "DIAGONAL",
			33 => "ARB_CHANNEL",
			34 => "PX_SUMM",
			35 => "MATRIX",
			36 => "GLASS_ALL",
			37 => "OBJOM_VSTROENNOY_PAMYATI",
			38 => "SIZE_ACCUMUL",
			39 => "FORM_FACTOR",
			40 => "DYSPLAY_CAM",
			41 => "SUMM_SIM",
			42 => "CHASTOTA",
			43 => "PERSONAL_SIZE",
			44 => "FORMAT_SIM",
			45 => "IMPEDANS",
			46 => "",
		),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DETAIL_SHOW_BASIS_PRICE" => "Y",
		"DETAIL_SHOW_MAX_QUANTITY" => "N",
		"DETAIL_STRICT_SECTION_CHECK" => "N",
		"DETAIL_USE_COMMENTS" => "Y",
		"DETAIL_USE_VOTE_RATING" => "N",
		"DETAIL_VK_API_ID" => "giomarket",
		"DETAIL_VK_USE" => "N",
		"DETAIL_VOTE_DISPLAY_AS_RATING" => "rating",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_ELEMENT_COUNT" => "N",
		"DISPLAY_ELEMENT_SELECT_BOX" => "N",
		"DISPLAY_SHOW_FILES_TYPE" => "new_window",
		"DISPLAY_TOP_PAGER" => "Y",
		"ELEMENT_SORT_FIELD" => $_GET['sort'],
		"ELEMENT_SORT_FIELD2" => "shows",
		"ELEMENT_SORT_ORDER" => $_GET['order'],
		"ELEMENT_SORT_ORDER2" => "asc",
		"FIELDS" => array(
			0 => "",
			1 => "",
		),
		"FILE_404" => "",
		"FILTER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"FILTER_OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_OFFERS_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_PRICE_CODE" => array(
			0 => "Для покупателей из интернет-магазина",
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "",
			1 => "WIDTH",
			2 => "LENGHT",
			3 => "HEIGHT",
			4 => "",
		),
		"FILTER_SKU_PHOTO" => "N",
		"FILTER_SKU_PHOTO_FLEX" => "N",
		"FILTER_VIEW_MODE" => "VERTICAL",
		"FORUM_ID" => "",
		"GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "3",
		"GIFTS_DETAIL_TAB_TITLE" => "Подарки",
		"GIFTS_HIDE_NOT_AVAILABLE" => "N",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_PLACE" => "TOP",
		"GIFTS_SECTION_LIST_BLOCK_TITLE" => "Подарки к товарам этого раздела",
		"GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT" => "3",
		"GROUP_PRICE_COUNT" => "count",
		"HANDLERS" => array(
			0 => "lj",
			1 => "twitter",
			2 => "vk",
			3 => "facebook",
			4 => "mailru",
			5 => "delicious",
		),
		"HIDE_FILTER_MOBILE" => "Y",
		"HIDE_NOT_AVAILABLE" => "L",
		"HIDE_OFFERS_LIST" => "N",
		"HIDE_PREVIEW_PROPS_INLIST" => "Y",
		"HOVER_MENU_COL_LG" => "1",
		"HOVER_MENU_COL_MD" => "1",
		"HOVER_MENU_COL_SM" => "1",
		"HOVER_MENU_COL_XS" => "1",
		"HOVER_TEMPLATE" => "list",
		"IBLOCK_ID" => "16",
		"IBLOCK_TYPE" => "1c_catalog2",
		"INCLUDE_SUBSECTIONS" => "Y",
		"IN_STOCK" => "В наличии",
		"LABEL_PROP" => "-",
		"LEFT_MENU_TEMPLATE" => "left",
		"LINE_ELEMENT_COUNT" => "3",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"LINK_IBLOCK_ID" => "16",
		"LINK_IBLOCK_TYPE" => "1c_catalog2",
		"LINK_PROPERTY_SID" => "ACCESSORIES",
		"LIST_BROWSER_TITLE" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_META_KEYWORDS" => "-",
		"LIST_OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"LIST_OFFERS_LIMIT" => "30",
		"LIST_OFFERS_PROPERTY_CODE" => array(
			0 => "PROIZVODITEL_1",
			1 => "SILENT",
			2 => "SIZE",
			3 => "COLOR",
			4 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "CML2_ARTICLE",
			1 => "METAL_DETECTOR",
			2 => "INSULATION",
			3 => "SUVENIR_NAZNACH",
			4 => "WIDTH",
			5 => "LENGHT",
			6 => "SIZE",
			7 => "HEIGHT",
			8 => "RECOMMEND",
			9 => "",
		),
		"MAIN_TITLE" => "Наличие на складах",
		"MAX_LEVEL" => "4",
		"MESSAGES_PER_PAGE" => "10",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "В КОРЗИНУ",
		"MESS_BTN_COMPARE" => "Сравнение",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_REQUEST" => "Оставить заявку",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"MIN_AMOUNT" => "10",
		"NOT_IN_STOCK" => "Под заказ",
		"NO_TABS" => "Y",
		"NO_WORD_LOGIC" => "N",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"OFFERS_SORT_FIELD" => "name",
		"OFFERS_SORT_FIELD2" => "shows",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "asc",
		"OFFERS_VIEW" => "SELECT",
		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
		"OFFER_PRICE_SHOW_FROM" => "N",
		"OFFER_TREE_PROPS" => array(
			0 => "CML2_MANUFACTURER",
		),
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => $PAGE_ELEMENT_COUNT,
		"PAGE_ELEMENT_COUNT_LIST" => array(
			0 => "",
			1 => "",
		),
		"PAGE_ELEMENT_COUNT_SHOW" => "Y",
		"PARTIAL_PRODUCT_PROPERTIES" => "Y",
		"PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
		"PICTURE_CATEGARIES" => "left",
		"PICTURE_SECTION" => "N",
		"PICTURE_SECTION_HOVER" => "N",
		"PREVIEW_DETAIL_PROPERTY_CODE" => array(
			0 => "PROPERTY_CML2_ARTICLE",
			1 => "PROPERTY_optom_vygodney",
			2 => "PROPERTY_MATERIAL",
			3 => "PROPERTY_VOLUME",
			4 => "PROPERTY_OTHER_ELEMENTS",
			5 => "PROPERTY_PROIZVODITEL_1",
			6 => "PROPERTY_country_of_manufacture",
			7 => "PROPERTY_price_per_capita",
			8 => "PROPERTY_OPT",
			9 => "PROPERTY_CML2_ATTRIBUTES",
			10 => "PROPERTY_Proizvoditeli",
			11 => "CML2_ATTRIBUTES",
			12 => "COLOR",
			13 => "",
		),
		"PREVIEW_TRUNCATE_LEN" => "",
		"PRICE_CODE" => array(
			0 => "Для покупателей из интернет-магазина",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PROPS_TAB_VIEW" => "LIST",
		"QTY_LESS_GOODS_TEXT" => "мало",
		"QTY_MANY_GOODS_INT" => "10",
		"QTY_MANY_GOODS_TEXT" => "много",
		"QTY_SHOW_TYPE" => "TEXT",
		"RCM_COUNT_DETAIL" => "3",
		"RCM_NAME_DETAIL" => "Также Вам может понадобиться",
		"RCM_TYPE_DETAIL" => "similar_sell",
		"RESTART" => "Y",
		"REVIEW_AJAX_POST" => "N",
		"ROOT_MENU_TYPE" => "left",
		"SECTIONS_MAIN_VIEW_MODE" => "LIST",
		"SECTIONS_SHOW_DESCRIPTION" => "N",
		"SECTIONS_SHOW_PARENT_NAME" => "Y",
		"SECTIONS_VIEW_MODE" => "LIST",
		"SECTION_ADD_TO_BASKET_ACTION" => "BUY",
		"SECTION_BACKGROUND_IMAGE" => "-",
		"SECTION_COUNT_ELEMENTS" => "N",
		"SECTION_ID_VARIABLE" => "SECTION_CODE",
		"SECTION_TOP_DEPTH" => "10",
		"SEF_FOLDER" => "/catalog/",
		"SEF_MODE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"SHORTEN_URL_KEY" => "",
		"SHORTEN_URL_LOGIN" => "",
		"SHOWS_BIGDATA_DETAIL" => "N",
		"SHOW_404" => "Y",
		"SHOW_CATALOG_QUANTITY" => "Y",
		"SHOW_CATALOG_QUANTITY_CNT" => "N",
		"SHOW_DEACTIVATED" => "N",
		"SHOW_DESCRIPTION_AFTER_SECTION" => "Y",
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"SHOW_EMPTY_STORE" => "Y",
		"SHOW_GENERAL_STORE_INFORMATION" => "Y",
		"SHOW_GIFTS_DETAIL_NOTICE" => "N",
		"SHOW_LEFT_CATALOG_MENU" => "Y",
		"SHOW_LEFT_MENU" => "Y",
		"SHOW_LEFT_MENU_SETTINGS" => "Y",
		"SHOW_LINK_TO_FORUM" => "N",
		"SHOW_MAIN_INSTEAD_NF_SKU" => "Y",
		"SHOW_MEASURE" => "N",
		"SHOW_OFFER_PIC_BYCLICK" => "Y",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_PRICE_NAME" => "Y",
		"SHOW_SECTION_DESC" => "top",
		"SHOW_SECTION_SEO" => "Y",
		"SHOW_TOP_ELEMENTS" => "Y",
		"SIDEBAR_DETAIL_SHOW" => "Y",
		"SIDEBAR_PATH" => "",
		"SIDEBAR_SECTION_SHOW" => "Y",
		"SKU_PROPS_SHOW_TYPE" => "square",
		"SKU_SORT_PARAMS" => "N",
		"STORES" => "",
		"STORE_PATH" => "/company/#store_id#",
		"STYLE_MENU" => "colored_light",
		"STYLE_MENU_HOVER" => "colored_light",
		"SUBMENU" => "ACTIVE_SHOW",
		"TEMPLATE_THEME" => "site",
		"THEME" => "default",
		"TILE_SHOW_PROPERTIES" => "Y",
		"TITLE_MENU" => "Каталог",
		"TOP_ADD_TO_BASKET_ACTION" => "BUY",
		"TOP_ELEMENT_COUNT" => "2",
		"TOP_ELEMENT_SORT_FIELD" => "shows",
		"TOP_ELEMENT_SORT_FIELD2" => "shows",
		"TOP_ELEMENT_SORT_ORDER" => "asc",
		"TOP_ELEMENT_SORT_ORDER2" => "asc",
		"TOP_LINE_ELEMENT_COUNT" => "3",
		"TOP_OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"TOP_OFFERS_LIMIT" => "5",
		"TOP_OFFERS_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"TOP_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"TOP_ROTATE_TIMER" => "30",
		"TOP_TITLE" => "Лидеры продаж",
		"TOP_VIEW_MODE" => "BANNER",
		"URL_TEMPLATES_READ" => "",
		"USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"USE_ALSO_BUY" => "N",
		"USE_BIG_DATA" => "N",
		"USE_CAPTCHA" => "Y",
		"USE_COMMON_SETTINGS_BASKET_POPUP" => "Y",
		"USE_COMPARE" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_EXT" => "Y",
		"USE_FAVORITES" => "N",
		"USE_FAVORITES_TEXT" => "Отложить",
		"USE_FILTER" => "Y",
		"USE_GIFTS_DETAIL" => "N",
		"USE_GIFTS_SECTION" => "Y",
		"USE_LANGUAGE_GUESS" => "Y",
		"USE_LINKS_SKU" => "Y",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_MIN_AMOUNT" => "N",
		"USE_ONE_CLICK" => "Y",
		"USE_ONE_CLICK_TEXT" => "Купить в 1 клик",
		"USE_PRICE_COUNT" => "Y",
		"USE_PRODUCT_QUANTITY" => "Y",
		"USE_REVIEW" => "Y",
		"USE_SALE_BESTSELLERS" => "N",
		"USE_SHARE" => "N",
		"USE_SHARE_TEXT" => "Поделиться",
		"USE_STORE" => "N",
		"USE_STORE_PHONE" => "Y",
		"USE_STORE_SCHEDULE" => "Y",
		"VIDEO_PLAYER" => "BITRIX",
		"VIDEO_PLAYER_FULLSCREEN" => "N",
		"VIDEO_TYPE" => "GRID",
		"VIEWED_PRODUCTS_BLOCK_TITLE" => "Просмотренные товары",
		"VIEWED_PRODUCTS_CNT" => "3",
		"VIEWED_PRODUCTS_SHOW" => "Y",
		"VIEWED_PRODUCTS_SORT" => "100",
		"VIEWED_PRODUCTS_WERE_SHOW" => "bottom",
		"ZOOM_ON" => "N",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE#/",
			"element" => "element/#ELEMENT_CODE#/",
			"compare" => "compare.php?action=#ACTION_CODE#",
			"smart_filter" => "#SECTION_CODE#/filter/#SMART_FILTER_PATH#/apply/",
		),
		"VARIABLE_ALIASES" => array(
			"compare" => array(
				"ACTION_CODE" => "action",
			),
		)
	),
	false
);?><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
