<?php
if(empty($_REQUEST['ID']))
	return;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!CModule::IncludeModule("catalog"))
	return;

// Получаем переменные
$arResult["ID"] = intval($_REQUEST['ID']); // ID товара для которого нужно получить торговые предложения
$arResult["NAME"] = $_REQUEST['NAME']; // Название товара для фильтрации
$arResult["PREVIEW_PICTURE"] = $_REQUEST['PREVIEW_PICTURE']; //Привью главного товара

// Получаем предложения
$arSKU = CCatalogSKU::getOffersList(
    array($arResult["ID"]),
    0,
    array('ACTIVE' => 'Y'),
    array('ID', 'NAME', 'CODE','SORT','PREVIEW_PICTURE','DETAIL_PICTURE'),
    array()
);

if(isset($arSKU[$arResult["ID"]])){
	
	// Формируем нормальный массив
	foreach($arSKU[$arResult["ID"]] as $item) {
		$key++;
		
		$arResult["OFFERS"][$key] = array(
			'ID'=> $item['ID'],
			'NAME'=> $item['NAME'],
			'CODE'=> $item['CODE'],
			'SORT'=> $item['SORT'],
		);
		
		if(!empty($item['PREVIEW_PICTURE']))
			$arResult["OFFERS"][$key]['PREVIEW_PICTURE'] = array(
					'ID'=> $item['PREVIEW_PICTURE'],
					'SRC'=> CFile::GetPath($item['PREVIEW_PICTURE']),
					);
					
		if(!empty($item['DETAIL_PICTURE']))
			$arResult["OFFERS"][$key]['DETAIL_PICTURE'] = array(
					'ID'=> $item['DETAIL_PICTURE'],
					'SRC'=> CFile::GetPath($item['DETAIL_PICTURE']),
					);
	}
}else{
	exit();
}
?>
<form method="post" action="<?=$_SERVER['HTTP_REFERER'];?>">
	<div id="basket-popup-product-image">
		<img src="<?=$arResult["PREVIEW_PICTURE"];?>" alt="">
	</div>
	<div id="basket-popup-product-name" class="basket-popup-name"><?=$arResult["NAME"];?></div>
	
	<?php 
	// Если размер 1, то скрываем выбор
	if(count($arResult["OFFERS"])<=1){?>
	<style>.size{opacity:0}</style>
	<?php }?>
	
	<div class="size">
		<p class="razmer">Выберите размер:</p>
		<div class="select">
		<select name="select" id="select">
			<?php
			foreach($arResult["OFFERS"] as $offer){
				// Обрезаем название (взято из детатальной страницы)
				$offer["NAME"] = trim(str_replace([$arResult['NAME']],'',$offer["NAME"]));
				if(mb_strpos($offer["NAME"],'(') === 0)
					 $offer["NAME"] = mb_substr($offer["NAME"],1, mb_strlen($offer["NAME"]) -2);
				 
				$picture = !empty($offer['DETAIL_PICTURE']['SRC'])? $offer['DETAIL_PICTURE']['SRC'] : $offer['PREVIEW_PICTURE']['SRC'];
				$picture = !empty($picture)? $picture : $arResult["PREVIEW_PICTURE"];
				?>
				<option value="<?=$offer['ID'];?>" data-src="<?=$picture;?>"><span><?=$offer['NAME'];?></span></option>
			<?php }?>		
		</select>	
		</div>
	</div>
	
	<div id="basket-popup-buttons">
		<form class="bxr-basket-action bxr-basket-group bxr-currnet-torg">
			<input type="hidden" name="quantity" value="1" class="bxr-quantity-text hidden-xs">
			<button class="bxr-color-button bxr-color-button-small-only-icon bxr-basket-add btn">
				<span class="fa fa-shopping-cart"></span> В корзину
			</button>
			<input class="bxr-basket-item-id" type="hidden" name="item" value="<?=$arResult["ID"];?>">
			<input type="hidden" name="action" value="add">
		</form>
		<button class="bxr-color-button bxr-corns btn" onclick="PixelClose();return false;"><span class="fa fa-undo" aria-hidden="true"></span>Продолжить покупки</button>
		<a class="bxr-color-button  bxr-corns btn" href="/personal/order/make/"><span class="fa fa-check-square-o" aria-hidden="true"></span>Оформить заказ</a>
	</div>
</form>
<script>
(function() {
	$('#basket-popup-product-image img').attr('src',$('#select option:selected').attr('data-src'));
	$('#basket-popup-buttons .bxr-basket-item-id').val($('#select option:selected').val());
	
	// Меняем размер
	$( "#select" ).change(function() {
		$('#basket-popup-product-image img').attr('src',$('#select option:selected').attr('data-src'));
		$('#basket-popup-buttons .bxr-basket-item-id').val($('#select option:selected').val());
	});

})();	
function PixelClose(){
	$( "#popup-message" ).css('display','none');
	$( ".popup-window-overlay" ).css('display','none');
	$('#popup-message .popup-success').html('Загрузка...');
}
</script>