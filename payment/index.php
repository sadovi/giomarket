<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Способы оплаты | ГиО Маркет");
$APPLICATION->SetTitle("Способы оплаты");
?><div class="bxr-payment-block">
 <span style="font-size: 14pt;"><i>Наличный расчёт</i></span>
	<hr>
 <img width="135" alt="payment-by-cash-for-express-delivery-flat-illustration.jpg" src="/upload/medialibrary/850/85063037fbb5db9c3668041c571bfe82.jpg" height="135" title="payment-by-cash-for-express-delivery-flat-illustration.jpg"><br>
	 Если товар доставляется курьером, то оплата осуществляется наличными курьеру в руки. <br>
</div>
<div>
	 Если вы забираете товар самостоятельно из офиса (м. Дубровка), оплата производится наличными.
</div>
<div>
 <br>
</div>
<div>
 <br>
</div>
 <i><span style="font-size: 14pt;">Банковской картой</span></i>
<hr>
<p style="color: #1b1b1b; font-weight: 400;">
 <strong><span style="font-size: 12pt;"><img width="70" alt="logo-visa.png" src="/upload/medialibrary/0a6/0a6d3c30133e408a82e0f56b6f4b335f.png" height="20" title="logo-visa.png">&nbsp;<img width="77" alt="logo-mastercard.png" src="/upload/medialibrary/b24/b24aa3caa8cc50629eb7e902490e8401.png" height="60" title="logo-mastercard.png">&nbsp;<img width="70" alt="logo_mir.jpg" src="/upload/medialibrary/b34/b341bd9126996d69e8413d20e4003ca7.jpg" height="20" title="logo_mir.jpg"></span></strong> <br>
	 Для выбора оплаты товара с помощью банковской карты на соответствующей странице необходимо нажать кнопку<strong> </strong><i>"</i><i>Оплата заказа банковской картой"</i>. Оплата происходит через ПАО СБЕРБАНК с использованием банковских карт следующих платёжных систем:
</p>
 <span style="color: #1b1b1b; font-weight: 400;"> </span>
<ul style="color: #1b1b1b; font-weight: 400;">
	<li> МИР (<em>разместить логотип МИР</em>);<br>
 </li>
	<li> VISA International (<em>разместить логотип VISA International</em>);<br>
 </li>
	<li> Mastercard Worldwide (<em>разместить логотип Mastercard Worldwide</em>).</li>
</ul>
<div>
	 Для оплаты (ввода реквизитов вашей карты) вы будете перенаправлены на платёжный шлюз ПАО СБЕРБАНК. Соединение с платёжным шлюзом и передача информации осуществляется в защищённом режиме с использованием протокола шифрования SSL. В случае если ваш банк поддерживает технологию безопасного проведения интернет-платежей Verified By Visa или MasterCard SecureCode, для проведения платежа также может потребоваться ввод специального пароля. Настоящий сайт поддерживает 256-битное шифрование. Конфиденциальность сообщаемой персональной информации обеспечивается ПАО СБЕРБАНК. Введённая информация не будет предоставлена третьим лицам за исключением случаев, предусмотренных законодательством РФ. Проведение платежей по банковским картам осуществляется в строгом соответствии с требованиями платёжных систем МИР, Visa Int. и MasterCard Europe Sprl.&nbsp;
</div>
<div>
 <br>
</div>
<div>
	 Возврат переведённых средств, производится на банковский счёт в течение 5-30 рабочих дней (срок зависит от банка, который выдал вашу банковскую карту). <br>
</div>
<div>
	 Затраты на доставку компенсируются покупателем при отказе от получения заказа.
</div>
 <u>Внимание! </u><u>Медицинские изделия не подлежат возврату</u>, так как такие изделия входят в Перечень товаров, не подлежащих возврату или обмену, согласно Постановлению от 19.01.96 № 55.
<ul style="color: #1b1b1b; font-weight: 400;">
</ul>
<p class="pay-text">
</p>
<div>
 <br>
 <i><span style="font-size: 14pt;">Наложенный платеж</span></i><strong><span style="font-size: 13pt;"> </span></strong>
	<hr>
</div>
<div>
 <img width="180" alt="сдэк наложенный платеж.png" src="/upload/medialibrary/1f1/1f1f023854ee2cc3c5d282996a269a18.png" height="100" title="сдэк наложенный платеж.png"><br>
	 Если товар доставляется курьерской службой СДЭК, Вы можете оплатить заказ при получении.<sup>1</sup>
</div>
<div>
	 NB! За наложенный платеж курьерская служба взимает 3% комиссию со всей стоимости заказа (товар+доставка).<br>
</div>
<div>
 <span style="font-size: 13pt;"><sup><span style="font-size: 9pt;">1 </span></sup></span><span style="font-size: 9pt;">если доставка реализуется по стандартному тарифу (не </span><span style="font-size: 9pt;">срочные отправления)</span>
</div>
<div>
 <br>
</div>
<div>
 <br>
</div>
<div>
 <strong><span style="font-size: 13pt;"><br>
 </span></strong>
</div>
 <i><span style="font-size: 14pt;">
Безналичный расчёт</span></i>
<hr>
<div>
	 &nbsp; <img width="90" alt="beznalinhi.png" src="/upload/medialibrary/482/48274a9918ad06103697ed3aee05507a.png" height="90" title="beznalinhi.png"><br>
</div>
<div>
	 Для выставления счета обратитесь к нашему менеджеру.
</div>
<div class="pay-wrap">
	 Реквизиты для выставления счета:<br>
	<table cellspacing="0" cellpadding="0" border="1">
	<tbody>
	<tr>
		<td>
			 Наименование организации <br>
		</td>
		<td>
			 Общество с ограниченной ответственностью <br>
			 «ГИО МАРКЕТ»
		</td>
	</tr>
	<tr>
		<td>
			 Юридический адрес
		</td>
		<td rowspan="2">
			 115088, Москва, улица Шарикоподшипниковская, д. 22, оф. 421/1
		</td>
	</tr>
	<tr>
		<td>
			 Фактический адрес <br>
		</td>
	</tr>
	<tr>
		<td>
			 Почтовый адрес <br>
		</td>
		<td>
			 115088, Москва, улица Шарикоподшипниковская, д. 22, оф. 421/1
		</td>
	</tr>
	<tr>
		<td>
			 ИНН
		</td>
		<td>
			 9723029640 от 31.05.2017 <br>
		</td>
	</tr>
	<tr>
		<td>
			 КПП
		</td>
		<td>
			 772301001 <br>
		</td>
	</tr>
	<tr>
		<td>
			 ОГРН
		</td>
		<td>
			 1177746540751 <br>
		</td>
	</tr>
	<tr>
		<td>
			 Расчетный счет
		</td>
		<td>
			 40702810638000049964 <br>
		</td>
	</tr>
	<tr>
		<td>
			 Наименование банка <br>
		</td>
		<td>
			 МОСКОВСКИЙ БАНК ПАО СБЕРБАНК № 9038/01793
		</td>
	</tr>
	<tr>
		<td>
			 БИК
		</td>
		<td>
			 044525225 <br>
		</td>
	</tr>
	<tr>
		<td>
			 Кор. счет
		</td>
		<td>
			 30101810400000000225 <br>
		</td>
	</tr>
	<tr>
		<td>
			 Код ОКПО
		</td>
		<td>
			 15925761 <br>
		</td>
	</tr>
	<tr>
		<td>
			 Код ОКВЭД
		</td>
		<td>
			 51.70 <br>
		</td>
	</tr>
	<tr>
		<td>
			 Код ОКТМО <br>
		</td>
		<td>
			 45396000000
			<p>
			</p>
		</td>
	</tr>
	</tbody>
	</table>
 <br>
	<div class="pay-desc">
 <br>
		 Для <b>онлайн оплаты</b> мы предлагаем оплату по указанным выше реквизитам в любом интернет-банкинге с указанием номера заказа в назначении платежа. Предварительно свяжитесь с нашим менеджером <a href="#SITE_DIR#/forms/index.php?ID=#IBLOCK_ID#">по телефону</a> или <a href="mailto:hello@giomarket.ru">по почте</a>.
	</div>
</div>
 <br>
 <span style="font-size: 15pt;">Инструкция для оплаты счета через <b>Сбербанк Онлайн</b><br>
 <br>
 <span style="font-size: 12pt;"> </span><span style="font-size: 11pt;">1. Зайдите в раздел "Переводы и платежи"</span><br>
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">2. Перейдите в раздел "Перевод организации"</span><br>
 <br>
 </span><img width="549" alt="СБЕР1.png" src="/upload/medialibrary/6fa/6fabc9b91dcb3bdd75e52c85d28233b3.png" height="298" title="СБЕР1.png"><br>
 <br>
 3. Заполните три поля:
<ul>
	<li>
	Номер счета 40702810638000049964 <br>
 </li>
	<li>ИНН 9723029640&nbsp;</li>
	<li>БИК 044525225&nbsp;</li>
	<li>выбираем из списка счет, с которого будут списаны денежные средства <br>
 </li>
</ul>
 <img width="700" alt="СБЕР2.png" src="/upload/medialibrary/6c7/6c73e4caea3d5e3b659631053c3333c4.png" height="594" title="СБЕР2.png"><br>
 <br>
 4. После нажатия на кнопку "Продолжить", появится следующее окно, в котором надо указать свои ФИО<br>
 <br>
 <img width="684" alt="СБЕР3.png" src="/upload/medialibrary/e11/e11006c60f9b8994ccb8891cf02e659f.png" height="664" title="СБЕР3.png"><br>
 5. В следующем окне указываются:<br>
<ul>
	<li>Адрес плательщика (можно только город)</li>
	<li>Назначение платежа: оплата по счету ***, где *** - номер счета</li>
</ul>
 6. Далее укажите сумму платежа.<br>
 7. По окончании оплаты сохраните квитанцию об оплате и пришлите ее на нашу электронную почту <a href="mailto:hello@giomarket.ru">hello@giomarket.ru</a><br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>