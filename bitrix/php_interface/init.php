<?php
    include_once 'include/brands.php';


AddEventHandler('catalog', 'OnProductUpdate', ['MyClass', 'OnProductUpdateHandler']);

class MyClass
{
    function OnProductUpdateHandler($id, $arFields)
    {
        CModule::IncludeModule('catalog');

        if($arFields['IBLOCK_ID'] == 17){
            $mxResult = CCatalogSku::GetProductInfo(
                $id, 17
            );
            if($mxResult['ID']){
                $res = CCatalogSKU::getOffersList(
                    $mxResult['ID']
                );
                foreach ($res as $idProduct => $offers){
                    $available = false;
                    foreach($offers as $offer){
                        $o = CCatalogProduct::GetByID($offer['ID']);
                        if($o['QUANTITY'] > 0){
                            $available = true;
                            break;
                        }
                    }
                    if($available)
					{CIBlockElement::SetPropertyValuesEx($idProduct, 16, array('AVAILABLE' => 547));}
					else
					{CIBlockElement::SetPropertyValuesEx($idProduct, 16, array('AVAILABLE' => ''));}
                }
            }
        }

    }
}

?>