<?
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("AddBrandsClass", "OnBeforeIBlockElementAddHandler"));
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("AddBrandsClass", "OnBeforeIBlockElementAddHandler"));

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

class AddBrandsClass
{
    const CATALOG_IBLOCK_ID = 16; //вместо 12 подставить ID инфоблока Каталога
    const BRAND_PROP_ID = 169; //вместо 124 подставить ID свойства Брендов, в которое изначально записывается значение
    const BRANDS_HB_ID = 3; //вместо 2 подставить ID highload-блока брендов
    const BRANDS_PROP_ID = 184; //вместо 78 подставить ID свойства Брендов, в которое будет записано значение
                
    public static function translit($str) {
        $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я',' ','(',')','.',',','/','"','\'','+','"');
        $lat = array('a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', '', 'y', '', 'e', 'yu', 'ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', '', 'y', '', 'e', 'yu', 'ya', '-', '', '','-','-','-','','','-','');
        return str_replace($rus, $lat, $str);
    }
    
    function OnBeforeIBlockElementAddHandler(&$arFields)
    {
        if($arFields["IBLOCK_ID"] == self::CATALOG_IBLOCK_ID)
        {
            $elementId = $arFields['ID'];
            $brandName = '';

            foreach ($arFields['PROPERTY_VALUES'][self::BRAND_PROP_ID] as $value) 
            {                
               if (strlen($value['VALUE'])>0)
                    $brandName = $value['VALUE'];
            }
            
            $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>self::CATALOG_IBLOCK_ID, "PROPERTY_ID"=>self::BRAND_PROP_ID));
            $arProp = array();
            while($enum_fields = $property_enums->GetNext())
            {                   
                $arProp[$enum_fields["ID"]] = $enum_fields["VALUE"];
            }

            if(isset($arProp[$brandName]) && !empty($arProp[$brandName]))
                $brandName = $arProp[$brandName];
                
            if (strlen($brandName)>0)
            {
                CModule::IncludeModule('highloadblock');
                CModule::IncludeModule('iblock');

                global $APPLICATION;

                $hlblock = HL\HighloadBlockTable::getById(self::BRANDS_HB_ID)->fetch();
                $entity = HL\HighloadBlockTable::compileEntity($hlblock);
                $entity_data_class = $entity->getDataClass();
                
                $name = trim($brandName);
                $link_name = AddBrandsClass::translit($name);
                $link = '/brands/'.$link_name.'/';
    
                $vendors_res = $entity_data_class::getList(array(
                    'filter' => array('UF_XML_ID' => $link_name),
                    'select' => array('ID'),
                    'order' => array()
                ));
                
                if($vendors_ob = $vendors_res->Fetch()) 
                {}
                else
                {
                    $result = $entity_data_class::add(array(
                        'UF_NAME'     => $name,
                        'UF_XML_ID'   => $link_name,
                        'UF_LINK'     => $link
                    ));
                }

                foreach ($arFields['PROPERTY_VALUES'][self::BRANDS_PROP_ID] as $k => $value) {
                    $arFields["PROPERTY_VALUES"][self::BRANDS_PROP_ID][$k] = array('VALUE' => $link_name);
                }              
                
                return true;
            }
        }
        return;
    }
}
?>