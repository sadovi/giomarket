<? IncludeTemplateLangFile(__FILE__);?>        </div>
    </div>
</div>


    <?if ($APPLICATION->GetCurPage(true) == SITE_DIR.'index.php'):?>

        <?if ($mainPageType == "one_col"):?>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "named_area",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => SITE_DIR."include/main_page_footer.php",
                            "INCLUDE_PTITLE" => GetMessage("GHANGE_MAIN_PAGE_FOOTER")
                        ),
                        false
                    );?>
                    </div>
                </div>
            </div>
        <?endif;?>

        <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.brandblock", 
	"brand_slider", 
	array(
		"IBLOCK_TYPE" => "1c_catalog2",
		"IBLOCK_ID" => "16",
		"ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],
		"ELEMENT_CODE" => $_REQUEST["ELEMENT_CODE"],
		"PROP_CODE" => array(
			0 => "MANUFACTURER",
			1 => "",
		),
		"WIDTH" => "120",
		"HEIGHT" => "50",
		"WIDTH_SMALL" => "120",
		"HEIGHT_SMALL" => "50",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "Y",
		"COMPONENT_TEMPLATE" => "brand_slider",
		"SHOW_DEACTIVATED" => "Y",
		"SINGLE_COMPONENT" => "N",
		"ELEMENT_COUNT" => "999",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false,
	array(
		"ACTIVE_COMPONENT" => "Y"
	)
);?>
    <?endif;?>


<?if (isset($BXReady)):?>
    <?$BXReady->showBannerPlace("BOTTOM");?>
<?endif;?>




<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter43585394 = new Ya.Metrika({
                    id:43585394,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/43585394" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->





    <footer>
        <div class="footer-line"></div>
        <div class="container footer-head">
            <div class="row">
                <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">Информация</div>
                <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">Каталог</div>
                <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">Производители</div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">Контакты</div>
            </div>
        </div>
        <style>.no-bold a { font-weight:  normal !important; }</style>
        <div class="container no-bold">
            <div class="row footerline" style="font-size: 13px; white-space: nowrap;">
                <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                    <a href="/company/">О магазине</a><br/>
                    <a href="/company/actions/">Акции</a><br/>
                    <a href="/guarantee/">Сертификация продукции</a><br/>
                    <a href="/delivery/">Доставка</a><br/>
                    <a href="/payment/">Оплата</a><br/>
                    <a href="/company/vacancies/">Вакансии</a><br/>
                    <a href="/company/news/">Новости</a><br/>
                    <a href="/articles/">Полезные статьи</a><br/><br/>
<a href="https://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2508/*https://market.yandex.ru/shop/407071/reviews"><img alt="Читайте отзывы покупателей и оценивайте качество магазина на Яндекс.Маркете" src="https://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2506/*https://grade.market.yandex.ru/?id=407071&action=image&size=1" width="120" height="110" border="0"></a>
<a href="https://webmaster.yandex.ru/siteinfo/?site=https://www.giomarket.ru"><img width="88" height="31" alt="" border="0" src="https://yandex.ru/cycounter?https://www.giomarket.ru&theme=light&lang=ru"/></a>
                </div>
                <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                    <?
                    CModule::IncludeModule('iblock');
                    $r = CIBlockSection::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => 16, 'ACTIVE' => 'Y', 'SECTION_ID' => false));
                    while ($ar = $r->GetNext()) {
                          ?><a href="<?=$ar['SECTION_PAGE_URL']?>"><?=$ar['NAME']?></a><br/><?
                       }
                    ?>
                </div>
                <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                        <?
                        if (CModule::IncludeModule('highloadblock'))
                        {
                           $arHLBlock = Bitrix\Highloadblock\HighloadBlockTable::getById(2)->fetch();
                           $obEntity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
                           $strEntityDataClass = $obEntity->getDataClass();

                           $rsData = $strEntityDataClass::getList(array(
                              'select' => array('ID','UF_NAME','UF_LINK'),
                              'order' => array('UF_SORT' => 'ASC'),
                              'limit' => '20',
                           ));
                           while ($arItem = $rsData->Fetch()) {
                              ?><a href="<?=$arItem['UF_LINK']?>"><?=$arItem['UF_NAME']?></a><br/><?
                           }
                        }
                        ?>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    г. Москва, ул. Шарикоподшипниковская, 22<br>
                    Телефон:<b> +7 (495) 363-98-05</b><br/>
                    Email: <a href="mailto:hello@giomarket.ru" style="text-decoration: underline;">hello@giomarket.ru</a><br/><br/>
                    <img alt="Снимок экрана 2016-12-28 в 16.46.14.png" src="/upload/medialibrary/9bd/9bd2a31205825b349b010670014aa3ea.jpg" title="logo-gio-market.png" width="271" height="124">
                </div>
            </div>
            <div style="text-align: center; padding: 15px 0;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> © <?=date('Y')?> - GioMarket.ru - Все права защищены</div>
            </div>
        </div>
    </footer>

    <?$formFrame = new \Bitrix\Main\Page\FrameHelper("iblock_form");
        $formFrame->begin();?>
        <?$APPLICATION->IncludeComponent(
	"alexkova.market:form.iblock", 
	".default", 
	array(
		"IBLOCK_TYPE" => "forms",
		"IBLOCK_ID" => "10",
		"STATUS_NEW" => "NEW",
		"USE_CAPTCHA" => "N",
		"USER_MESSAGE_ADD" => GetMessage("FORM_ANSWER_RESULT"),
		"RESIZE_IMAGES" => "N",
		"MODE" => "link",
		"PROPERTY_CODES" => array(
			0 => "47",
			1 => "48",
			2 => "49",
		),
		"NAME_FROM_PROPERTY" => "48",
		"GROUPS" => array(
			0 => "2",
		),
		"MAX_FILE_SIZE" => "0",
		"EVENT_CLASS" => "open-answer-form",
		"BUTTON_TEXT" => "",
		"POPUP_TITLE" => GetMessage("RECALL_MESSAGE"),
		"SEND_EVENT" => "KZNC_NEW_FORM_PHONE_RESULT",
		"COMPONENT_TEMPLATE" => ".default",
		"PERSONAL_DATA" => "N",
		"PERSONAL_DATA_TEXT" => "Cогласен на обработку персональных данных в соответсвии с положением",
		"PERSONAL_DATA_CAPTION" => "Положение",
		"PERSONAL_DATA_URL" => "",
		"PERSONAL_DATA_ERROR" => "Обработка вашей заявки без согласия на обработку персональных данных невозможна.",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
        <?$formFrame->beginStub();
        echo " ";
        $formFrame->end();?>
        <?$formFrame = new \Bitrix\Main\Page\FrameHelper("iblock_form_request");
        $formFrame->begin();?>
        <?$APPLICATION->IncludeComponent(
	"alexkova.market:form.iblock", 
	"request_trade", 
	array(
		"IBLOCK_TYPE" => "forms",
		"IBLOCK_ID" => "11",
		"STATUS_NEW" => "NEW",
		"USE_CAPTCHA" => "N",
		"USER_MESSAGE_ADD" => GetMessage("FORM_ANSWER_RESULT"),
		"RESIZE_IMAGES" => "N",
		"MODE" => "link",
		"PROPERTY_CODES" => array(
			0 => "51",
			1 => "52",
			2 => "53",
			3 => "54",
			4 => "55",
			5 => "56",
			6 => "57",
			7 => "58",
		),
		"NAME_FROM_PROPERTY" => "56",
		"GROUPS" => array(
			0 => "2",
		),
		"MAX_FILE_SIZE" => "0",
		"EVENT_CLASS" => "bxr-trade-request",
		"BUTTON_TEXT" => "",
		"POPUP_TITLE" => GetMessage("REQUEST_TRADE"),
		"SEND_EVENT" => "KZNC_NEW_FORM_REQUEST_RESULT",
		"COMPONENT_TEMPLATE" => "request_trade",
		"PERSONAL_DATA" => "N",
		"PERSONAL_DATA_TEXT" => "Cогласен на обработку персональных данных в соответсвии с положением",
		"PERSONAL_DATA_CAPTION" => "Положение",
		"PERSONAL_DATA_URL" => "",
		"PERSONAL_DATA_ERROR" => "Обработка вашей заявки без согласия на обработку персональных данных невозможна.",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
        <?$formFrame->beginStub();
        echo " ";
        $formFrame->end();?>
        <?$formFrame = new \Bitrix\Main\Page\FrameHelper("iblock_form_one_click_buy");
        $formFrame->begin();?>
        <?$APPLICATION->IncludeComponent(
	"alexkova.market:form.iblock", 
	"request_trade", 
	array(
		"IBLOCK_TYPE" => "forms",
		"IBLOCK_ID" => "9",
		"STATUS_NEW" => "NEW",
		"USE_CAPTCHA" => "N",
		"USER_MESSAGE_ADD" => GetMessage("FORM_ANSWER_RESULT"),
		"RESIZE_IMAGES" => "N",
		"MODE" => "link",
		"PROPERTY_CODES" => array(
			0 => "39",
			1 => "40",
			2 => "41",
			3 => "42",
			4 => "43",
			5 => "44",
			6 => "45",
			7 => "46",
			8 => "340",
			9 => "422",
		),
		"NAME_FROM_PROPERTY" => "44",
		"GROUPS" => array(
			0 => "2",
		),
		"MAX_FILE_SIZE" => "0",
		"EVENT_CLASS" => "bxr-one-click-buy",
		"BUTTON_TEXT" => "",
		"POPUP_TITLE" => GetMessage("ONE_CLICK_FORM_TITLE"),
		"SEND_EVENT" => "KZNC_NEW_FORM_CLICK_RESULT",
		"COMPONENT_TEMPLATE" => "request_trade",
		"PERSONAL_DATA" => "N",
		"PERSONAL_DATA_TEXT" => "Cогласен на обработку персональных данных в соответсвии с положением",
		"PERSONAL_DATA_CAPTION" => "Положение",
		"PERSONAL_DATA_URL" => "",
		"PERSONAL_DATA_ERROR" => "Обработка вашей заявки без согласия на обработку персональных данных невозможна.",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
        <?$formFrame->beginStub();
        echo " ";
        $formFrame->end();?>
<script type="text/javascript"> //<![CDATA[ 
var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
//]]>
</script>
    </body>
</html>