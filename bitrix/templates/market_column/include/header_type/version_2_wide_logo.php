<div class="bxr-full-width bxr-container-headline head_v2_wide_logo <?=($BXReady->inverseHead) ? 'bxr-inverse':''?>">
    <div class="container">
        <div class="row headline">
         <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 bxr-v-autosize">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "named_area",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR."include/logo.php",
                        "INCLUDE_PTITLE" => GetMessage("GHANGE_LOGO")
                    ),
                    false
                );?>
            </div>
            <div class="col-lg-6 col-md-6 hidden-sm hidden-xs bxr-v-autosize">
                <?
                $APPLICATION->IncludeComponent(
	"alexkova.market:search.title", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"NUM_CATEGORIES" => "2",
		"TOP_COUNT" => "8",
		"ORDER" => "date",
		"USE_LANGUAGE_GUESS" => "Y",
		"CHECK_DATES" => "N",
		"SHOW_OTHERS" => "N",
		"PAGE" => "#SITE_DIR#catalog/",
		"SHOW_INPUT" => "Y",
		"INPUT_ID" => "title-search-input",
		"CONTAINER_ID" => "title-search",
		"CATEGORY_0_TITLE" => "Товары",
		"CATEGORY_0" => array(
			0 => "iblock_1c_catalog2",
		),
		"PRICE_CODE" => array(
			0 => "Для покупателей из интернет-магазина",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "200",
		"SHOW_PREVIEW" => "Y",
		"CONVERT_CURRENCY" => "Y",
		"PREVIEW_WIDTH" => "75",
		"PREVIEW_HEIGHT" => "75",
		"CATEGORY_0_iblock_catalog" => array(
			0 => "12",
		),
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CATEGORY_0_iblock_1c_catalog" => array(
			0 => "15",
		),
		"CATEGORY_0_iblock_1c_catalog2" => array(
			0 => "17",
		),
		"CURRENCY_ID" => "RUB",
		"CATEGORY_0_iblock_offers" => array(
			0 => "all",
		),
		"CATEGORY_1_TITLE" => "",
		"CATEGORY_1" => array(
			0 => "iblock_1c_catalog2",
		),
		"CATEGORY_1_iblock_1c_catalog2" => array(
			0 => "17",
		)
	),
	false
);
                ?>
            </div>
            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs bxr-v-autosize">
                <?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	"include_with_btn", 
	array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/phone.php",
		"INCLUDE_PTITLE" => GetMessage("GHANGE_PHONE"),
		"COMPONENT_TEMPLATE" => "include_with_btn",
		"SHOW_BTN" => "Y",
		"BTN_TYPE" => "LINK",
		"BTN_CLASS" => "recall-btn open-answer-form",
		"FLOAT" => "NONE",
		"LINK_TEXT" => "Заказать обратный звонок",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
            </div>
            <div class="hidden-lg hidden-md col-sm-6 col-xs-6 bxr-v-autosize" id="bxr-basket-mobile">

            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>            