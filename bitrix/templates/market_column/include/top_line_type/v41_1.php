<?$_SESSION["BXR_BASKET_TEMPLATE"] = "fixed";?>
<div class="bxr-full-width bxr-top-headline">
	<div class="container">
		<div class="row  bxr-basket-row">
			<div class="col-sm-2 col-xs-2 hidden-lg hidden-md hidden-sm bxr-mobile-login-area">
				<div class="bxr-counter-mobile hidden-lg hidden-md bxr-mobile-login-icon">
					<i class="fa fa-phone"></i>
				</div>
			</div>
			<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
				<div class="bxr-top-line-auth text-right" style="line-height: 30px;">
					Адрес магазина: 115008, г. Москва, ул. Шарикоподшипниковская, 22
				</div>
			</div>
			<div class="col-sm-2 col-xs-2 hidden-lg hidden-md hidden-sm bxr-mobile-phone-area">
				<div class="bxr-counter-mobile hidden-lg hidden-md bxr-mobile-phone-icon">
					<i class="fa fa-user"></i>
				</div>
			</div>

			<div class="col-lg-5 col-md-8 hidden-sm hidden-xs">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "named_area",
                                Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => SITE_DIR."include/topline.php",
                                    "INCLUDE_PTITLE" => GetMessage("GHANGE_TOP_LINE_TEXT")
                                ),
                                false
                            );?>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 text-right">
                            <?$basketFrame = new \Bitrix\Main\Page\FrameHelper("bxr_small_basket");
                            $basketFrame->begin();?>
                            <?$APPLICATION->IncludeComponent(
                                "alexkova.market:basket.small", 
                                "gio.custom", 
                                array(
                                    "COMPONENT_TEMPLATE" => "gio.custom",
                                    "PATH_TO_BASKET" => SITE_DIR."personal/basket/",
                                    "PATH_TO_ORDER" => SITE_DIR."personal/order/",
                                    "USE_COMPARE" => "N",
                                    "IBLOCK_TYPE" => "catalog",
                                    "IBLOCK_ID" => "",
                                    "USE_DELAY" => "Y",
                                    "PRODUCT_PROVIDER_CLASS" => "",
                                    "MOBILE_BLOCK" => "bxr-basket-mobile",
                                    "COMPOSITE_FRAME_MODE" => "A",
                                    "COMPOSITE_FRAME_TYPE" => "AUTO"
                                ),
                                false
                            );?>
                            <?$basketFrame->beginStub();
                            $basketFrame->end();?>
                        </div>
                    </div>


			<div class="clearfix"></div>
		</div>
	</div>
</div>

