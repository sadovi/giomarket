(function (window) {
    jQuery.easing['jswing'] = jQuery.easing['swing'];

    jQuery.extend( jQuery.easing,
        {
           easeOutBounce: function (x, t, b, c, d) {
                if ((t/=d) < (1/2.75)) {
                    return c*(7.5625*t*t) + b;
                } else if (t < (2/2.75)) {
                    return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
                } else if (t < (2.5/2.75)) {
                    return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
                } else {
                    return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
                }
            },
            easeOutElastic: function (x, t, b, c, d) {
                var s=1.70158;var p=0;var a=c;
                if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
                if (a < Math.abs(c)) { a=c; var s=p/4; }
                else var s = p/(2*Math.PI) * Math.asin (c/a);
                return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
            },
            easeOutExpo: function (x, t, b, c, d) {
                return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
            }
        });


    window.BXReady = {
        showAjaxShadow: function(element, idArea, localeShadow){

            if (localeShadow == true){
                $(element).addClass('ajax-shadow');
                $(element).addClass('ajax-shadow-r');
            }
            else{
                if ($('div').is('#'+idArea)){

                }
                else
                {
                    $('<div id="'+idArea+'" class="ajax-shadow"></div>').appendTo('body');
                }

                $('#'+idArea).show();
                $('#'+idArea).width($(element).width());
                $('#'+idArea).height($(element).outerHeight());
                $('#'+idArea).css('top', $(element).offset().top+'px');
                $('#'+idArea).css('left', $(element).offset().left+'px');
            }

        },

        closeAjaxShadow: function(idArea, localShadow){
            if (localShadow == true){
                $(idArea).removeClass('ajax-shadow-r');
                $(idArea).removeClass('ajax-shadow');
            }
            else{
                $('#'+idArea).hide();
            }
        },

        scrollTo: function(targetElement){

            $("html, body").animate({
                scrollTop: $(targetElement).offset().top-20 + "px"
            }, {
                duration: 500
            });
        },

        autosizeVertical: function(){
            maxHeight = 0;
            $('div.bxr-v-autosize').each(function(){
                if ($(this).height()> maxHeight){
                    maxHeight = $(this).height();
                };
            });
            $('div.bxr-v-autosize').each(function(){

                    delta = Math.round((maxHeight - $(this).height())/2);
                    $(this).css({'padding-top': delta+'px', 'padding-bottom': delta+'px'});
            });
        }
    };

    window.BXReady.Market = {
        loader: [],
        
        setPriceCents: function(){

            $('.bxr-format-price').each(function(){
                price = $(this).html();

                newPrice = price.replace(/(\.\d\d)/g, '<sup>$1</sup>');
                newPrice = newPrice.replace(/(\.)/g, '');

                $(this).html(newPrice);
            });
        },

        bestsellersAjaxUrl: '/ajax/bestsellers.php',
        markersAjaxUrl: '/ajax/markers.php'
    };
    
    $(document).on('click', '.search-btn', function() {
        var search = $('#searchline');
        if(search.is(":visible"))
            search.fadeOut();
        else
            search.fadeIn(); 
    });

    $(document).on('click', '.bxr-mobile-login-icon', function() {
        $('.bxr-mobile-login-area').fadeOut(200, function(){
            $('.bxr-mobile-phone-area').fadeIn(200);
        });
    });

    $(document).on('click', '.bxr-mobile-phone-icon', function() {
        $('.bxr-mobile-phone-area').fadeOut(200, function(){
            $('.bxr-mobile-login-area').fadeIn(200);
        });
    });

    $(window).on ('resize', function() {
        if ($(window).width() > 960) {
            $('.bxr-mobile-phone-area').fadeOut(200, function(){
                $('.bxr-mobile-login-area').fadeIn(200);
            });
	} else {
            $('.bxr-mobile-phone-area').fadeIn(200, function(){
                $('.bxr-mobile-login-area').fadeOut(200);
            });
	}
    });
    
    $(document).on('click', '.mobile-footer-menu-tumbl', function() {
        $(this).next().toggle();
    });
    
    $(document).on('click', '.delivery-item-more', function() {
        if ($(this).prev('.delivery-item-text').css('display') == 'none'){
            $(this).prev('.delivery-item-text').slideDown();
            $(this).html('<span class="fa fa-angle-up"></span> скрыть информацию');
        } else {
            $(this).prev('.delivery-item-text').slideUp();
            $(this).html('<span class="fa fa-angle-down"></span> узнать больше');
        }
    });

//    window.onload = function() {
//        BXReady.autosizeVertical();
//    };
    
    window.onload = function()
    {
        if (typeof window.BXReady.Market.loader != 'object')
            window.BXReady.Market.loader = [];
        for ( var i in window.BXReady.Market.loader )
        {
            if ( typeof( window.BXReady.Market.loader[i] ) == 'function' ) window.BXReady.Market.loader[i](); 
        }
    };
    
    if (typeof window.BXReady.Market.loader != 'object')
            window.BXReady.Market.loader = [];
    window.BXReady.Market.loader.push(BXReady.autosizeVertical);

    
    $( window ).resize(function() {
        BXReady.autosizeVertical();
    });
})(window);

$(document).ready(function()
{
    $('.bx-breadcrumb').show().insertBefore('h1:first');
});

/* Заполняем форму при покупке в 1 клик */
$(function() {
	$(".pixel-click-buy").click(function(){
	
		var offerid = $(this).attr('data-ofitem');
		var id = $(this).attr('data-iditem');
		var name = $(this).attr('data-name');
		var link = $(this).attr('data-link');
		var price = $(this).attr('data-price');
		var place = $(this).attr('data-place');

        if($(this).closest('tr').length > 0){
            var offerName = $(this).closest('tr').find('.bxr-font-hover-light-span').html();
        }
        else{
            var offerName = $('.bxr-sku-select-chosen-inner .bxr-offer-props-name').html();
        }

		setTimeout(function () {

			if(offerid) document.querySelector('input[data-code=TRADE_ID_HIDDEN]').value = offerid;
			if(id) document.querySelector('input[data-code=OFFER_ID_HIDDEN]').value = id;
			if(name) document.querySelector('input[data-code=TRADE_NAME_HIDDEN]').value = name;
			if(link && document.querySelector('input[data-code=TRADE_LINK_HIDDEN]') !== null) document.querySelector('input[data-code=TRADE_LINK_HIDDEN]').value = link;
			if(price && document.querySelector('input[data-code=PRICE_ITEM_HIDDEN]') !== null)	document.querySelector('input[data-code=PRICE_ITEM_HIDDEN]').value = price;
			if(place)	document.querySelector('input[data-code=PLACE_HIDDEN]').value = place;
            if(offerName)	document.querySelector('input[data-code=OFFER_NAME]').value = offerName;


			setGoal('click-open'); // Цели метрики
			
			console.log('Данные товара в форму записаны');
		}, 1000);
		
	});
	
	// Покупка в листинге
	$('.pixel-buy').on('click', function(){
		var ID = $(this).attr('data-product-id');
		var NAME = $(this).attr('data-product-name');
		var PREVIEW_PICTURE = $(this).attr('data-product-photo');
		
		window.BX.ajax({
			url:'/ajax/market_buy.php',
			data: {ID: ID, first: 'Y', NAME: NAME, PREVIEW_PICTURE: PREVIEW_PICTURE},
			method: 'POST',
			async: true,
			onsuccess: function(data){
				$('#popup-message .popup-success').html(data);
			}
		});
		
		BX.ready(function () {
			var popup = BX.PopupWindowManager.create("popup-message", BX('element'), {
				autoHide: true,
				offsetLeft: 0,
				offsetTop: 0,
				overlay : true,
				draggable: {restrict:true},
				closeByEsc: true,
				closeIcon: { right : "12px", top : "8px"},
				content: '<div class="popup-success">Загрузка...</div>',

				width: 720, // ширина окна
				height: 520, // высота окна
				zIndex: 100, // z-index
				closeIcon: {
					opacity: 1
				},
				titleBar: 'Купить товар',
				closeByEsc: true, // закрытие окна по esc
				darkMode: false, // окно будет светлым или темным
				autoHide: true, // закрытие при клике вне окна
				draggable: true, // можно двигать или нет
				resizable: true, // можно ресайзить
				angle: false, // появится уголок
				overlay: {
					// объект со стилями фона
					backgroundColor: 'black',
					opacity: 500
				}, 
				events: {
				   onPopupShow: function() {
				   },
				   onPopupClose: function() {
					  $('#popup-message .popup-success').html('Загрузка...');
				   }
				}
			});

			popup.show();
		});
		return false;
	});
});

jQuery(document).on('click','.bxr-basket-add',function() {
	setTimeout(function () {
		$('#basketPopup .btn-items-pixel').remove();
		
		$('#basketPopup .bxr-list').find('div.t_1').each(function(){
			
			var href = $(this).find('.bxr-element-name a').attr('href');
			
			$(this).find('.bxr-element-action').html('<a href="'+href+'" class="bxr-color-button pixel-buy">Подробнее</a>');
			
		});	
	}, 1000);  
});	