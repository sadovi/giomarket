/*
* Функция для удобной обработке целей Яндекс.Метрика от Сергея Князева
* Код счётчика: 43585394
*/

function setGoal(events){
	yaCounter43585394.reachGoal(events);
	console.log('Params: '+events+' Event: OK');
}
$(function() {
	
	// Цель: Купить в 1 клик
	$('#bxr-market-detail-basket-btn-wrap .bxr-one-click-buy').click(function(){
		setGoal('click-open');
	});
	
	// Цель: Купить в 1 клик - Отправили форму
	$("#iblockForm9").submit(function() {
	  setGoal('click-sent');
	});
	
	// Цель: Купить в 1 клик МИНИ
	$('.basket-line-qty .bxr-one-click-buy').click(function(){
		setGoal('mini-call');
	});

	// Цель: Добавили в корзину
	$('#bxr-market-detail-basket-btn-wrap .bxr-basket-add').click(function(){
		setGoal('add-to-basket');
	});
	
	// Цель: Добавили в корзину МИНИ
	$('.basket-line-qty .bxr-basket-add').click(function(){
		setGoal('mini-basket');
	});
	
});