<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->createFrame()->begin('sortpanel');
global $arSortGlobal;
$numShow = ('Y' == $arParams["PAGE_ELEMENT_COUNT_SHOW"]);
$viewShow = ('Y' == $arParams["CATALOG_VIEW_SHOW"]);

$arSort = array(
    array(
        'SORT' => 'NAME',
        'ORDER' => 'ASC',
        'NAME' => 'названию',
    ),
    array(
        'SORT' => 'PROPERTY_MINIMUM_PRICE',
        'ORDER' => 'ASC',
        'NAME' => 'возрастанию цены',
    ),
    array(
        'SORT' => 'PROPERTY_MINIMUM_PRICE',
        'ORDER' => 'DESC',
        'NAME' => 'убыванию цены',
    ),
    array(
        'SORT' => 'TIMESTAMP_X',
        'ORDER' => 'DESC',
        'NAME' => 'новинкам',
    ),
);

$SORT = $_GET['sort'];
$ORDER = $_GET['order'];
?>
<style>
.col-md-9.col-sm-8 > .row {
    margin: 0;
}
.sort-line {
    background: #f5f5f5;
    padding: 7px 10px;
    margin-bottom: 15px;
    font-size: 13px;
}
.sort-line span,
.sort-line a {
    margin-right: 5px;
}

.sort-line .count-on-page {
    float: right;
}
.sort-line .count-on-page span,
.sort-line .count-on-page a {
    margin-right: 0;
    margin-left: 5px;
}
</style>
<div class="sort-line hidden-xs hidden-sm">
    <span>Сортировать по:</span>
    <?
    foreach ($arSort as $k => $ar)
    {
        $link = $APPLICATION->GetCurPageParam((($ar['SORT'] == 'NAME' && $ar['ORDER'] == 'ASC') ? '' : 'sort='.$ar['SORT'].'&order='.$ar['ORDER']), array('sort', 'order'));

        if ($SORT == $ar['SORT'] && $ORDER == $ar['ORDER'])
        {
            ?><span style="font-weight: bold;"><?=$ar['NAME']?></span><?
        }
        else
        {
            ?><a href="<?=$link?>"><?=$ar['NAME']?></a><?
        }
        if ($ar['SORT'] != 'TIMESTAMP_X')
            echo '<span class="sep">/</span>';
    }
    ?>

    <?
    $ar_count = array(
        12 => 12,
        30 => 30,
        60 => 60,
        'all' => 'все'
    );
    ?>
    <div class="count-on-page hidden-md">
        <span>На странице:</span>
        <?foreach ($ar_count as $k => $v):?>
            <?if ($arParams['PAGE_ELEMENT_COUNT'] == $k || ($k == 'all' && !isset($ar_count[$arParams['PAGE_ELEMENT_COUNT']]))):?>
                <span style="font-weight: bold;"><?=$v?></span>
            <?else:?>
                <a href="<?=$APPLICATION->GetCurPageParam('PAGE_ELEMENT_COUNT='.$k, array('PAGE_ELEMENT_COUNT', 'PAGEN_1'))?>"><?=$v?></a>
            <?endif?>
        <?endforeach?>
        <span>Всего:</span>
        <span class="catalog-count-all" style="font-weight: bold;">..</span>
    </div>
</div>

<?
return;
?>
<div class="col-xs-12<?=($arParams["THEME"] == 'default') ?' bxr-border-color':' bxr-color-flat'?> bxr-sort-panel">
        <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-5 text-xs-right text-md-left">
                <span class="hidden-sm hidden-lg hidden-md text-xs-right"><?=GetMessage("KZNC_SORT_BY")?></span>
                <?foreach ($arResult["SORT_PROPS"] as $key => $val):
                        $className = ($arSortGlobal["sort"] == $val[0]) ? ' active' : ' hidden-xs';
                        $icon = "";
                        if ($className){
                                $className .= ($arSortGlobal["sort_order"] == 'asc') ? ' asc' : ' desc';
                                $icon = ($arSortGlobal["sort_order"] == 'asc') ? '<i class="fa fa-arrow-up"></i>' : ' <i class="fa fa-arrow-down"></i>';
                        }

                        if (strlen($val[3])>0){
                                $className .= " ".$val[3];
                        }

                        $newSort = ($arSortGlobal["sort"] == $val[0]) ? ($arSortGlobal["sort_order"] == 'desc' ? 'asc' : 'desc') : $arAvailableSort[$key][1];
                        ?>
                        <a href="<?=$APPLICATION->GetCurPageParam('sort='.$key.'&order='.$newSort, array('sort', 'order'))?>"
                           class="bxr-sortbutton<?=$className?> <?if(number_key($arResult["SORT_PROPS"], $key) == count($arResult["SORT_PROPS"])) echo "last";?>" rel="nofollow">
                                <?=$val[2]?><?=($arSortGlobal["sort"] == $val[0])?$icon:''?>
                        </a>
                <?endforeach;?>
        </div>
        <?if ($numShow || true):?>
                <div class="col-xs-12 col-sm-<?=$viewShow?'6':'12'?> col-md-<?=$viewShow?'4':'7'?> text-right">
                        <span class="hidden-sm hidden-lg hidden-md"><?=GetMessage("KZNC_SORT_COUNT_NAME")?></span>
                        <? foreach ($arParams["PAGE_ELEMENT_COUNT_LIST"] as $val) :?>
                        <?if ($val > 0):?>
                        <a href="<?=$APPLICATION->GetCurPageParam('num='.$val,array('num'));?>" title="<?=GetMessage('KZNC_VIEW_BY')." ".$val." ".GetMessage('KZNC_ITEM_NAME').NumberWordEndingsEx($val)?>" class="bxr-view-mode<?=($arSortGlobal["num"] == $val) ? ' active' : '';?>">
                                <?=$val;?>
                        </a>
                        <?endif;?>
                        <?endforeach;?>
                </div>
        <?endif;?>
        <?if ($viewShow):?>
        <div class="col-xs-12 col-sm-<?=$numShow?'6':'12'?> col-md-<?=$numShow?'3':'7'?> text-right">
                <span class="hidden-sm hidden-lg hidden-md"><?=GetMessage("KZNC_SORT_VIEW_NAME")?></span>
                <a href="<?=$APPLICATION->GetCurPageParam('view=title',array('view'));?>" title="<?=GetMessage('KZNC_VIEW_PLITKA')?>" class="bxr-view-mode<?=($arSortGlobal['view'] == 'title' || !$arSortGlobal['view']) ? ' active' : '';?>">
                        <i class="fa fa-th"></i>
                </a>
                <a href="<?=$APPLICATION->GetCurPageParam('view=list',array('view'));?>" title="<?=GetMessage('KZNC_VIEW_LIST')?>" class="bxr-view-mode<?=($arSortGlobal['view'] == 'list') ? ' active' : '';?>">
                        <i class="fa fa-th-list"></i>
                </a>
                <a href="<?=$APPLICATION->GetCurPageParam('view=table',array('view'));?>" title="<?=GetMessage('KZNC_VIEW_TABLE')?>" class="bxr-view-mode<?=($arSortGlobal['view'] == 'table') ? ' active' : '';?>">
                        <i class="fa fa-align-justify"></i>
                </a>
        </div>
        <?endif;?>
        </div>
</div>