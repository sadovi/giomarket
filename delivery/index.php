<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Доставка заказов | ГиО Маркет");
$APPLICATION->SetTitle("Доставка заказов");
?><div>
</div>
<h2 style="text-align: center;">МОСКВА и МО:</h2>
 <i><span style="font-size: 14pt;">Самовывоз<br>
 </span></i>
<hr>
 <br>
 <img width="60" alt="dolly-300x300.png" src="/upload/medialibrary/308/3086bbc343e5d2d1fb31b4139c5a35d6.png" height="60" title="dolly-300x300.png"><br>
 <br>
 СРОК: 1 РАБ. ДЕНЬ<br>
 <br>
 Доступен для заказов, подтвержденных менеджером интернет-магазина.&nbsp;Чтобы ознакомиться с более подробной информацией о расположении нашего офиса, перейдите по ссылке в раздел <a href="https://www.giomarket.ru/company/contacts/">"Контакты"</a>.<br>
 Самовывоз осуществляется по будням с 10:00 до 18:00<br>
<div class="bxr-delivery-block">
	<p class="delivery-text">
	</p>
	<div class="delivery-wrap">
		<div class="delivery">
 <span style="font-size: 14pt;"><i>
			<div>
				 Курьером
			</div>
 </i></span>
			<div class="delivery-desc">
				<div class="delivery-desc-item">
					<div class="delivery-img">
						<hr>
 <img width="70" alt="circle.png" src="/upload/medialibrary/690/69042d09d69c3992d51e6d4119f02cbf.png" height="70" title="circle.png"><br>
 <br>
						 СРОК: 1 РАБ. ДЕНЬ<br>
 <br>
						 СТОИМОСТЬ: <br>
						 Доставка по Москве в пределах МКАД&nbsp;- 400 руб.*, за МКАДом&nbsp;в пределах 10 км - 600 руб.&nbsp;<br>
 <b>Бесплатная доставка</b> при заказе на сумму <b>от</b><b> 6000 руб.</b><b>&nbsp;</b>по Москве в пределах МКАД.<br>
 <br>
						 * Заказы доставляются курьером при весе заказа не более 6 кг. Доставка производится в рабочие дни с 11:00 до 21:00 и осуществляется по согласованию с курьером.
						<p>
							 Габаритные заказы и заказы свыше 6 кг передаются на доставку в транспортную компанию СДЭК, при этом сроки доставки могут быть изменены.&nbsp;&nbsp;
						</p>
						<h2 style="text-align: center;">РОССИЯ И СНГ:</h2>
 <br>
 <img width="100" alt="сдэк.png" src="/upload/medialibrary/562/5627bf48418088480675dab4bb12da12.png" height="42" title="сдэк.png"><br>
 <br>
						 СРОК: от 1 РАБ. ДНЯ (не считая дня забора заказа)<br>
 <br>
						 КАК ДОСТАВЯТ:<br>
						 Экспресс-доставка СДЭК доставит Ваш заказ в самые короткие сроки <b>по всему миру</b>.<br>
						<ul>
							<li>"Посылка склад-дверь" - доставка курьером до указанного адреса<br>
 </li>
							<li>"Посылка склад-склад" - доставка заказа до пункта выдачи (адреса пунктов выдачи в Вашем городе смотрите ниже): <?$GLOBALS['APPLICATION']->IncludeComponent("ipol:ipol.sdekPickup",".default",array(),false);?>&nbsp;</li>
						</ul>
 <br>
						 Вы можете самостоятельно рассчитать стоимость доставки с помощью калькулятора (ниже):<br>
						 NB! Каждый заказ мы застраховываем от его потери и порчи. Стоимость страхования груза составляет 1%.<br>
					</div>
					 <iframe id="cdek-calc" src="https://cdek-online.ru/construct_form.php?city_from=44&sender_status=ecommerce&method_from=1&cod=1" scrolling="no" frameborder="0" height="443" width="300"></iframe>
					<div class="delivery-img">
 <br>
					</div>
					<div class="delivery-img">
 <b><br>
 </b><b>ИНФОРМИРОВАНИЕ О ЗАКАЗЕ:</b><br>
						 Перед доставкой с Вами связываются по телефону, указанному в заказе.<br>
						 Отследить заказ Вы можете <a href="https://www.cdek.ru/track.html">на сайте СДЭК</a>. Номер накладной приходит на указанную Вами электронную почту автоматически, или уточните номер у наших менеджеров.<br>
 <a href="https://www.cdek.ru/track.html">https://www.cdek.ru/track.html</a><br>
 <br>
					</div>
				</div>
			</div>
			<div class="delivery-desc">
				<div class="delivery-img">
					<div class="delivery-item-text" style="display: block;">
 <b><u>
						ВНИМАНИЕ!</u></b><br>
						 Затраты на доставку компенсируются покупателем при отказе от получения заказа. Сумма к оплате при отказе от получения посылки = (стоимость доставки + 1% страхование груза ) х 1,5.<br>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <b><br>
 </b><b>ОБРАЩАЕМ ВАШЕ ВНИМАНИЕ: медицинские изделия&nbsp;не подлежат&nbsp;возврату, так как такие изделия входят в Перечень товаров, не подлежащих возврату или обмену, согласно <a href="http://www.consultant.ru/document/cons_doc_LAW_17579/"><span style="color: #000000;">Постановлению от 19.01.96 № 55</span></a>.</b><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>