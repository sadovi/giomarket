<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Ошибка 404. Страница не найдена.");
?><p style="text-align: center;">
 <span style="font-size: 36pt;">Ошибка 4</span><img alt="404.png" src="/upload/medialibrary/d33/d33ba8585110b5d34cc344c3c7e8a0ea.png" title="404.png" width="50" height="106"><span style="font-size: 72pt;"><span style="font-size: 36pt;">4</span> </span>
</p>
<p>
</p>
<p style="text-align: center;">
 <span style="font-size: 14pt;">Извините, но такой страницы не существует, либо произошла страшная, трагическая ошибка.</span><br>
 <span style="font-size: 14pt;">
	Вы можете </span><span style="font-size: 14pt;"><a href="/">Перейти на главную</a></span><span style="font-size: 14pt;">, </span><span style="font-size: 14pt;"><a href="/catalog/">Перейти в каталог</a></span><span style="font-size: 14pt;"> или </span><span style="font-size: 14pt;"><a href="javascript:history.go(-1)">Вернуться назад</a></span><span style="font-size: 14pt;"> на страницу, с которой пришли.</span>
</p>
<p>
</p>
<p>
</p>
<div class="clear">
</div>
 <br>
&nbsp;
	<?$APPLICATION->IncludeComponent(
	"alexkova.market:catalog.markers",
	"template1",
	Array(
		"ACTION_VARIABLE" => "action",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"BASKET_URL" => "/personal/basket.php",
		"BESTSELLER_IBLOCK_ID" => "14",
		"BESTSELLER_IBLOCK_TYPE" => "1c_catalog2",
		"BXREADY_ELEMENT_DRAW" => "system#ecommerce.v2.lite",
		"BXREADY_LIST_BOOTSTRAP_GRID_STYLE" => "12",
		"BXREADY_LIST_HIDE_MOBILE_SLIDER_ARROWS" => "N",
		"BXREADY_LIST_HIDE_MOBILE_SLIDER_AUTOSCROLL" => "Y",
		"BXREADY_LIST_HIDE_MOBILE_SLIDER_SCROLLSPEED" => "600",
		"BXREADY_LIST_HIDE_SLIDER_ARROWS" => "Y",
		"BXREADY_LIST_LG_CNT" => "3",
		"BXREADY_LIST_MD_CNT" => "3",
		"BXREADY_LIST_PAGE_BLOCK_TITLE" => "",
		"BXREADY_LIST_PAGE_BLOCK_TITLE_GLYPHICON" => "",
		"BXREADY_LIST_SLIDER" => "Y",
		"BXREADY_LIST_SLIDER_MARKERS" => "Y",
		"BXREADY_LIST_SM_CNT" => "4",
		"BXREADY_LIST_VERTICAL_SLIDER_MODE" => "N",
		"BXREADY_LIST_XS_CNT" => "6",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CONVERT_CURRENCY" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => "16",
		"IBLOCK_TYPE" => "1c_catalog2",
		"INCLUDE_SUBSECTIONS" => "Y",
		"OFFERS_CART_PROPERTIES" => "",
		"OFFERS_LIMIT" => "5",
		"OFFERS_PROPERTY_CODE" => array("CML2_ARTICLE"),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "30",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array("Для покупателей из интернет-магазина"),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_PRICE_COUNT" => "",
		"TAB_ACTION_SETTING" => "Y",
		"TAB_ACTION_SORT" => "100",
		"TAB_HIT_SETTING" => "N",
		"TAB_HIT_SORT" => "400",
		"TAB_NEW_SETTING" => "N",
		"TAB_NEW_SORT" => "300",
		"TAB_RECCOMEND_SETTING" => "N",
		"TAB_RECCOMEND_SORT" => "200",
		"USE_PRICE_COUNT" => "Y",
		"USE_PRODUCT_QUANTITY" => "N"
	),
false,
Array(
	'ACTIVE_COMPONENT' => 'Y'
)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>