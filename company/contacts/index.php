<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Контакная информация | ГиО Маркет");
$APPLICATION->SetTitle("Наши контакты");
?><span itemprop="name">ООО ГИО МАРКЕТ</span> <br>
 <br>
<div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
 <b>Адрес офиса</b>
	<div>
 <span itemprop="addressLocality">г. Москва</span>, <span itemprop="streetAddress">м. Дубровка, ул. Шарикоподшипниковская, 22, подъезд 4, этаж 2, домофон 41, офис 421</span>
	</div>
	<div>
 <br>
	</div>
 <b>Телефоны</b>
	<div>
 <span itemprop="telephone">+7 495 363–98–05, +7 917 578-25-22</span>
	</div>
 <br>
 <b>Электронная почта</b>
	<div>
 <span itemprop="email"><a href="mailto:hello@giomarket.ru">hello@giomarket.ru</a></span>
	</div>
</div>
 <br>
 <b>График работы</b>
<div>
 <time itemprop="openingHours" content="Mo-Fr 9:30-18:30">С понедельника по пятницу с 9:30 до 18:30</time>
</div>
 <br>
<div class="bxr-contacts-block">
	<div class="block">
 <label>Мы в социальных сетях</label>
		<div class="footer-socnet-wrap">
 <a href="https://vk.com/giomarket" target="_blank" class="footer-socnet vk"></a> <a href="https://www.facebook.com/giomarket.medical.supplies" target="_blank" class="footer-socnet facebook"></a> <a href="https://www.instagram.com/giomarket.ru/" target="_blank" class="footer-socnet instagram"></a>
		</div>
	</div>
	<div class="block">
 <label>и мессенджерах на связи в любое время</label> <a href="https://api.whatsapp.com/send/?phone=79175782522&text&app_absent=0"><img width="60" alt="wv.png" src="/upload/medialibrary/5fc/5fcecbcade7221ea91827c739b05b063.png" height="30" title="wv.png"></a>
	</div>
	<div class="block map">
		 <script type="text/javascript" charset="utf-8" data-skip-moving="true" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ab25113adb5d6423b30909f993f9462d21dfa5507bb65c9af02afa0678536653d&amp;width=100%&amp;height=470&amp;lang=ru_RU&amp;scroll=true"></script>
	</div>
	<div class="block">
 <label>от&nbsp;м. Дубровка (~3&nbsp;мин.)</label>
		<ol>
			<li>3ий вагон из&nbsp;центра;</li>
			<li>при выходе из&nbsp;стеклянных дверей поверните направо;</li>
			<li>при выходе из&nbsp;метро поверните налево;</li>
			<li>перейдя пешеходный переход дублера, поверните направо;</li>
			<li>через 10&nbsp;м поверните налево между домами 24 и 18;</li>
			<li>пройдя дом с&nbsp;вывеской «Нотариус Адвокаты», Вы&nbsp;увидите 5-тиэтажное здание бежевого цвета&nbsp;— это и&nbsp;есть наш бизнес-центр.</li>
		</ol>
 <img width="307" alt="бц.jpg" src="/upload/medialibrary/231/23137b51b1a8c6bd3e764c2ee052715d.jpg" height="252" title="бц.jpg"><br>
 <img width="717" alt="бц2.JPG" src="/upload/medialibrary/4f5/4f510e742df9908bd1079c38aaa7ce0a.JPG" height="165" title="бц2.JPG"><br>
		<ol>
		</ol>
	</div>
	<div class="block">
 <label>от&nbsp;МЦК Дубровка (~12&nbsp;мин.)</label>
		<ol>
			<li>последний вагон (при движении по&nbsp;часовой стрелке);</li>
			<li>в&nbsp;наземном переходе поворот направо;</li>
			<li>при выходе из&nbsp;перехода поверните налево;</li>
			<li>перейдя пешеходный переход дублера, поверните направо;</li>
			<li>через 10&nbsp;м поверните налево между домами 24 и 18;</li>
			<li>пройдя дом с&nbsp;вывеской «Нотариус Адвокаты», Вы&nbsp;увидите 5-тиэтажное здание бежевого цвета&nbsp;— это и&nbsp;есть наш бизнес-центр</li>
		</ol>
 <img width="271" alt="бц3.png" src="/upload/medialibrary/4ae/4ae2a6b2cb39f9aa774ea0963bf8084c.png" height="307" title="бц3.png"><br>
		<ol>
		</ol>
	</div>
 <label><b>от&nbsp;м. Автозаводская (~10 мин.)&nbsp;</b></label>на троллейбусе 26 или&nbsp;автобусе 99
</div>
<ol>
	<li>выход в&nbsp;сторону&nbsp;ул. Мастеркова: последний вагон из центра, из стеклянных дверей направо, при выходе из метро&nbsp;налево (справа будет остановка)</li>
	<li>сесть на&nbsp;троллейбус 26&nbsp;или автобус 99&nbsp;и доехать до остановки "метро Дубровка"<br>
 </li>
	<li>перейти на противоположную сторону по переходу;</li>
	<li>при выходе из&nbsp;перехода поверните налево;</li>
	<li>перейдя пешеходный переход дублера, поверните направо;</li>
	<li>через 10&nbsp;м поверните налево между домами 24 и 18;</li>
	<li>пройдя дом с&nbsp;вывеской «Нотариус Адвокаты», Вы&nbsp;увидите 5-тиэтажное здание бежевого цвета&nbsp;— это и&nbsp;есть наш бизнес-центр</li>
</ol>
 <br>
 <b>от&nbsp;м. Пролетарская&nbsp;(~15 мин.)</b> на троллейбусе 26, трамвае 43, 12 или&nbsp;автобусе 9
<ol>
	<li>сесть на троллейбус 26, трамвай 43, 12&nbsp;или&nbsp;автобус 9&nbsp;и доехать до остановки "метро Дубровка"</li>
	<li>идите прямо и, перейдя пешеходный переход дублера, поверните направо;</li>
	<li>через 10&nbsp;м поверните налево между домами 24 и 18;</li>
	<li>пройдя дом с&nbsp;вывеской «Нотариус Адвокаты», Вы&nbsp;увидите 5-тиэтажное здание бежевого цвета&nbsp;— это и&nbsp;есть наш бизнес-центр</li>
</ol>
 <br>
 <br>
<div class="block">
 <label><b>Реквизиты для выставления счета</b></label>
	<table cellspacing="0" cellpadding="0" border="1">
	<tbody>
	<tr>
		<td>
			 Наименование организации
		</td>
		<td>
			 Общество с ограниченной ответственностью «ГИО МАРКЕТ»
		</td>
	</tr>
	<tr>
		<td>
			 Юридический адрес
		</td>
		<td rowspan="2">
			 115088, Москва, улица Шарикоподшипниковская, д. 22
		</td>
	</tr>
	<tr>
		<td>
			 Фактический адрес
		</td>
	</tr>
	<tr>
		<td>
			 Почтовый адрес
		</td>
		<td>
			 115088, Москва, улица Шарикоподшипниковская, д. 22, а/я 12
		</td>
	</tr>
	<tr>
		<td>
			 ИНН
		</td>
		<td>
			 9723029640 от 31.05.2017
		</td>
	</tr>
	<tr>
		<td>
			 КПП
		</td>
		<td>
			 772301001
		</td>
	</tr>
	<tr>
		<td>
			 ОГРН
		</td>
		<td>
			 1177746540751
		</td>
	</tr>
	<tr>
		<td>
			 Расчетный счет
		</td>
		<td>
			 40702810638000049964
		</td>
	</tr>
	<tr>
		<td>
			 Наименование банка
		</td>
		<td>
			 МОСКОВСКИЙ БАНК ПАО СБЕРБАНК № 9038/01793
		</td>
	</tr>
	<tr>
		<td>
			 БИК
		</td>
		<td>
			 044525225
		</td>
	</tr>
	<tr>
		<td>
			 Кор. счет
		</td>
		<td>
			 30101810400000000225
		</td>
	</tr>
	<tr>
		<td>
			 Код ОКПО
		</td>
		<td>
			 15925761
		</td>
	</tr>
	<tr>
		<td>
			 Код ОКВЭД
		</td>
		<td>
			 51.70
		</td>
	</tr>
	<tr>
		<td>
			 Код ОКТМО
		</td>
		<td>
			 45396000000
		</td>
	</tr>
	</tbody>
	</table>
</div>
 <br>
 <br>
 <br>
 <br>
 <br>
 <br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>