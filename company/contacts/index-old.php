<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Наши контакты");
?><div class="bxr-contacts-block">
 <img width="77" alt="Customer-Supprt.png" src="/upload/medialibrary/cfc/cfc746785e2aa0a3fe39bad2550c64b6.png" height="77" title="Customer-Supprt.png" align="left">Телефон: +7 495 363 98 05<br>
	 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Пн-Пт, 9:30-18:30<br>
 <br>
	 Email: <a href="mailto:hello@giomarket.ru">hello@giomarket.ru</a><br>
 <br>
	 Почтовый адрес: 115088,&nbsp;г. Москва, ул. Шарикоподшипниковская, 22, а/я 12.<br>
 <br>
 <b>Мы в социальных сетях:</b> <br>
	<div class="footer-socnet-wrap">
 <a href="https://vk.com/giomarket" target="_blank" class="footer-socnet vk"></a> <a href="https://www.facebook.com/giomarket.medical.supplies" target="_blank" class="footer-socnet facebook"></a> <a href="https://www.instagram.com/giomarket.ru/" target="_blank" class="footer-socnet instagram"></a>
	</div>
 <br>
 <b>и мессенджерах на связи в любое время:<br>
 <a href="mailto:8 917 578 25 22"><img width="60" alt="wv.png" src="/upload/medialibrary/5fc/5fcecbcade7221ea91827c739b05b063.png" height="30" title="wv.png"></a><br>
 <br>
	 Расположение ГиО Маркет:<br>
 </b><br>
	 г. Москва, м. Дубровка, ул. Шарикоподшипниковская, 22, подъезд 4, этаж 2, домофон 41, офис 421.
</div>
 <b>&nbsp;&nbsp;</b><?$APPLICATION->IncludeComponent(
	"bitrix:map.google.view",
	".default",
	Array(
		"API_KEY" => "AIzaSyAI_VQ9QjAfL9kQXmQx_dN-dAQxEPW1sHw",
		"COMPONENT_TEMPLATE" => ".default",
		"CONTROLS" => array(0=>"SMALL_ZOOM_CONTROL",1=>"TYPECONTROL",2=>"SCALELINE",),
		"INIT_MAP_TYPE" => "ROADMAP",
		"MAP_DATA" => "a:4:{s:10:\"google_lat\";d:55.718351650460164;s:10:\"google_lon\";d:37.67222654975535;s:12:\"google_scale\";i:17;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:4:\"TEXT\";s:79:\"ГиО Маркет###RN###ул. Шарикоподшипниковская, 22\";s:3:\"LON\";d:37.672926660379744;s:3:\"LAT\";d:55.71804745710976;}}}",
		"MAP_HEIGHT" => "300",
		"MAP_ID" => "",
		"MAP_WIDTH" => "100%",
		"OPTIONS" => array(0=>"ENABLE_SCROLL_ZOOM",)
	)
);?><br>
 <br>
 <img width="307" alt="бц.jpg" src="/upload/medialibrary/231/23137b51b1a8c6bd3e764c2ee052715d.jpg" height="252" title="бц.jpg"><img width="1024" alt="бц2.JPG" src="/upload/medialibrary/4f5/4f510e742df9908bd1079c38aaa7ce0a.JPG" height="236" title="бц2.JPG"><br>
<ol>
</ol>
 <br>
 от м. Дубровка (~3 мин.)<br>
<ol>
	<li>3ий вагон из центра&nbsp;</li>
	<li>при выходе из стеклянных дверей поверните направо</li>
	<li>при выходе из метро поверните налево&nbsp;</li>
	<li>перейдя пешеходный переход, поверните направо</li>
	<li>через 10 м поверните налево&nbsp;</li>
	<li>пройдя дом с вывеской "Нотариус Адвокаты", Вы увидите 5-тиэтажное&nbsp;здание бежевого цвета, огороженное зеленым забором - это и есть наш бизнес-центр</li>
</ol>
 &nbsp; <img width="512" alt="схема проезда3.png" src="/upload/medialibrary/3e2/3e28ede4d3a764c75c4633cde023a777.png" height="219" title="схема проезда3.png"><br>
 <br>
 от МЦК&nbsp;Дубровка (~12 мин.)<br>
<ol>
	<li>последний вагон (при движении по часовой стрелке) <br>
 </li>
	<li>в наземном переходе&nbsp;поворот направо</li>
	<li>при выходе из перехода первый поворот налево на ул. 2-я Машиностроения</li>
	<li>идти прямо до ул. Шарикоподшипниковская, 22 (5-тиэтажное&nbsp;здание бежевого цвета, огороженное зеленым забором - это и есть наш бизнес-цент)</li>
	<li>чтобы зайти в БЦ, необходимо обойти забор справа<br>
 </li>
</ol>
 <img width="271" alt="бц3.png" src="/upload/medialibrary/4ae/4ae2a6b2cb39f9aa774ea0963bf8084c.png" height="307" title="бц3.png"><br>
<ol>
</ol>
 от м. Автозаводская (~25 мин.) на 26 троллейбусе<br>
<ol>
	<li>выход в сторону ул. Мастеркова, идти по ней до&nbsp;ул. Восточная</li>
	<li>по ул. Восточная идти 950 м</li>
	<li>по ул. Новоостаповская идти еще 500 м</li>
	<li>повернуть направо на&nbsp;ул. Шарикоподшипниковская</li>
</ol>
 <br>
 &nbsp; <br>
 Если&nbsp;у вас появились вопросы, обращайтесь&nbsp;<a href="mailto:hello@giomarket.ru">в нашу техподдержку</a><a href="mailto:hello@giomarket.ru">.</a><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>