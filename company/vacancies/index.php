<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Вакансии");
?><div class="bxr-vacancy-block">
	<div class="vacancy-img">
 <img width="102" alt="Flat-Icons-12-24.png" src="/upload/medialibrary/e7b/e7b7350af901e3eda7cc31f1fd7064c1.png" height="102" title="Flat-Icons-12-24.png" align="top">Если Вы хотите присоединиться к нашей команде, пришлите свое&nbsp;резюме&nbsp;на <a href="mailto:hello@giomarket.ru">наш email</a>.
	</div>
</div>
 <br>
 Текущие открытые вакансии<b>:<br>
 </b><br>
<h2><span style="font-size: 13pt;">"Менеджер интернет-магазина"</span></h2>
<p>
	 Должностные обязанности:
</p>
 - Обработка входящих заказов из корзин;<br>
 - Прием входящих звонков, оформление заказов, консультирование покупателей по телефону, помощь в подборе товаров;<br>
 - Комплектация заказов и подготовка документов;<br>
 - Отправление заказов курьером/курьерской службой и контроль их получения.<br>
<p>
</p>
<p>
 <br>
</p>
<p>
	 Требования:
</p>
 - Образование медицинское, фармацевтическое, ветеринарное (желательно);<br>
 - Знания продукции по направлениям медицинские расходные материалы (желательно);<br>
- Опытный пользователь ПК и MS Office;<br>
- Грамотная устная и письменная речь;<br>
 - Опыт работы в интернет-магазине от 6 месяцев (желательно);<br>
 - Активность‚ коммуникабельность‚ ответственность‚ пунктуальность.<br>
<p>
</p>
<p>
 <br>
</p>
<p>
	 Условия:
</p>
 - Официальное оформление по ТК РФ; <br>
 - Оклад + % от продаж; <br>
 - По результатам работы годовая премия;<br>
 - Оплачиваемый отпуск.<br>
<p>
</p>
<p>
 <br>
</p>
<p>
	 Тип занятости:<br>
	 - Полная занятость: 5/2, с 9:30 до 18:30&nbsp; <br>
 <b> </b>
</p>
<h1><br>
 </h1>
<h1><b><span style="font-size: 13pt; color: #b7b7b7;">"Менеджер по продажам медицинских расходных материалов" </span></b></h1>
 <span style="color: #b7b7b7;"> </span><b><span style="color: #b7b7b7;"> </span></b><span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;">
	Должностные обязанности: </span><br>
 <span style="color: #b7b7b7;"> </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;">
	- Поиск новых клиентов, расширение клиентской базы; </span><br>
 <span style="color: #b7b7b7;"> </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;">
	- Регулярные встречи, переговоры с представителями частных медицинских учреждений; </span><br>
 <span style="color: #b7b7b7;"> </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;">
	- Согласование условий сотрудничества; </span><br>
 <span style="color: #b7b7b7;"> </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;">
	- Заключение договоров; - Отслеживание дебиторской задолженности; - 100% рабочего времени в «полях». </span><br>
 <span style="color: #b7b7b7;"> </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;"> </span><br>
 <span style="color: #b7b7b7;"> </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;">
	Требования: </span><br>
 <span style="color: #b7b7b7;"> </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;">
	- Опыт работы в продажах от 1 года, желательно в медицинской сфере; </span><br>
 <span style="color: #b7b7b7;"> </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;">
	- Навыки ведения переговоров, презентации продукции; </span><br>
 <span style="color: #b7b7b7;"> </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;">
	- Активность, нацеленность на результат; </span><br>
 <span style="color: #b7b7b7;"> </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;">
	- Желание работать и зарабатывать. </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;"> </span><br>
 <span style="color: #b7b7b7;"> </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;">
	Условия: </span><br>
 <span style="color: #b7b7b7;"> </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;">
	- Официальное оформление по ТК РФ; </span><br>
 <span style="color: #b7b7b7;"> </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;">
	- Оклад + % от продаж; </span><br>
 <span style="color: #b7b7b7;"> </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;">
	- Оплата корпоративной связи, компенсация транспортных расходов. </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;"> </span><b><br>
 <span style="color: #b7b7b7;"> </span></b><span style="color: #b7b7b7;"> </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;">
	Тип занятости: </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;">
	- Частичная </span><br>
 <span style="color: #b7b7b7;"> </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
 <span style="color: #b7b7b7;"> </span><br>
 <span style="color: #b7b7b7;"> </span>
</p>
 <span style="color: #b7b7b7;"> </span>
<p>
	 Контактная информация:
</p>
<p>
	 +7 (917) 578 25 22 или по эл. почте <a href="mailto:hello@giomaret.ru">hello@giomarket.ru</a>&nbsp;
</p>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>